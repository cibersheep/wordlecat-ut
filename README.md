# Wordle en català per l'Ubuntu Touch

[![OpenStore](https://open-store.io/badges/ca.svg)](https://open-store.io/app/wordlecat.cibersheep)

Endevina el mot en sis intents. Cada dia n'hi ha un de nou per endevinar.

## Informació
Aquest app ve de moltes fonts:
- De l'app per l'Ubuntu Touch https://gitlab.com/bradpitcher/ubuntu-touch-wordle/
- Del projecte a github https://github.com/rbndev/WordleCAT
  - que prové de l'eliminat https://github.com/GeloZP/WordleCAT/
    - que prové de l'original...
